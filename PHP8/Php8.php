<?php
//named arguments: truyền biến vào theo tham số thay vì vị trí
array_fill(start_index: 0, value: 10, count: 100);
htmlspecialchars(string: $string, double_encode: false);

//Constructor property promotion: định nghĩa thuộc tính trong hàm __construct

class Point
{
    public function __construct(
        public string $x = 'anh',
        public float $y = 0.0,
        public int $z = 1,
    ) {
    }
}


//Union Types: cho phép thuộc tính thuộc nhiều kiểu dữ liệu

class Number
{
    public function __construct(
        private int|float $number
    ) {
    }
}

new Number('aaaaa'); // lỗi
new Number(1.0); // oke ( 1.0 thuộc kiểu float)

//Match expression gần giống switch ko cần break. nó trả kết quả match đầu tiên

echo match (8.0) {
    '8.0' => "string",
    8.0 => "integer",
};


// có hỗ trợ gộp nhiều kết quả (or) không tìm được => throw UnhandledMatchError $e
$condition = 5;

try {
    match ($condition) {
        1, 2 => '1,2',
        3, 4 => '3,4',
        // default => 'day la default'
    };
} catch (\UnhandledMatchError $e) {
    var_dump($e);
}

//Nullsafe operator kiểm tra null  tiện hơn

$country = $user?->address?->getCity()?->country;

//cũ
$user =  null;

if ($user !== null) {
    $address = $user->address;

    if ($address !== null) {
        $city = $address->getCity();

        if ($city !== null) {
            $country = $city->country;
        }
    }
}