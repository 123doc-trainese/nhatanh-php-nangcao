# PHP nâng cao

## Lý thuyết

- Tìm hiểu về Magic method trong PHP OOP
- Tìm hiểu về autoload trong PHP
- Tìm hiểu thuật toán được sử dụng bên trong một số phương thức builtin php : sắp xếp, so sánh, tìm kiếm, thay thế, ...
- Tìm hiểu các điểm mới trong phiên bản PHP mới nhất so với các bản còn trong thời gian support
- Đọc https://github.com/jupeter/clean-code-php#single-responsibility-principle-srp

## Kiến thức tìm hiểu được

- Magic method là phương thức đặc biệt để tùy biến các sự kiện trong php, cung cấp thêm cách giải quyết vấn đề. Chậm hơn phương thức thường
- các Magic method trong php

- \_\_construct() : phương thức này được sử dụng khi khởi tạo đối tượng

  ```
  class a
  {
      public function __construct($text)
      {
          $this->text = $text;
          echo "khởi tạo đối tượng $text";
      }
  }
  $a = new a('nhatanh');

  // kết quả
  khởi tạo đối tượng nhatanh
  ```

- \_\_destruct() : phương thức này được sử dụng khi hủy đối tượng. Mặc định là khi kết thúc chương trình. Hoặc sử dụng phương thức hủy đối tượng

  ```
  class a
  {
      public function __destruct()
      {
          echo "sử dụng phương thức __destruct() ";
      }
  }
  $a = new a();
  unset($a);
  // kết quả
  sử dụng phương thức __destruct() "
  ```

- \_\_get(): phương thức này được sử dụng khi lấy dữ liệu từ thuộc tính

  ```
  class a
  {
      private $name = 'tran hoang nhat anh';

      public function __get($key)
      {
          echo "sử dụng phương thức __get()";
      }
  }
  $a = new a();
  $a->name;
  // kết quả
  sử dụng phương thức __get()
  ```

- \_\_set() : phương thức này được sử dụng khi `set` dữ liệu cho thuộc tính

  ```
  class a
  {
      public function __set($key, $value)
      {
          echo "sử dụng phương thức __set()";
  }
  }
  $a = new a();
  $a->name = "tran hoang nhat anh";
  // kết quả
  sử dụng phương thức __set()
  ```

- \_\_unset() : phương thức được sử dụng khi `unset` giá trị của thuộc tính

  ```
  class a
  {
      public function __unset($key)
      {
          echo "sử dụng phương thức __unset()";
      }
  }
  $a = new a();
  unset($a->name);
  // kết quả
  sử dụng phương thức __unset()
  ```

- \_\_call() : phương thức này được sử dụng khi sử dụng 1 `function` trong đối tượng

  ```
      class a
      {
          public function __call($method, $arguments)
          {
              echo "sử dụng phương thức __call()";
          }
      }
      $a = new a();
      $a->getInfo();

      // kết quả
      sử dụng phương thức __call()
  ```

- \_\_callStatic() : phương thức này được sử dụng khi sử dụng 1 `static function` trong đối tượng

  ```
   class a
      {
          public static function __callStatic($name, $arguments)
          {
              echo "sử dụng phương thức __callStatic()";
          }
      }
      $a = new a();
      $a::getInfo();

  // kết quả
  sử dụng phương thức __callStatic()
  ```

- \_\_isset() : phương thức này được sử dụng khi kiểm tra `isset` , `empty` thuộc tính trong đối tượng

  ```
  class a
  {
      public function __isset($name)
      {
          echo "sử dụng phương thức __isset()";
      }
  }
  $a = new a();
  isset($a->name);
  // kết quả
  sử dụng phương thức __isset()
  ```

- \_\_toString() : phương thức này được sử dụng khi dùng đối tượng như 1 chuỗi

  ```
   class a
  {
      public function __toString()
      {
          return "sử dụng phương thức __toString()";
      }
  }
  $a = new a();
  echo $a;
  // kết quả
  sử dụng phương thức __toString()
  ```

- \_\_invoke() : phương thức này được sử dụng khi dùng đối tượng như hàm

  ```
  class a
  {
      public function __invoke()
      {
          echo "sử dụng phương thức __invoke()";
      }
  }
  $a = new a();
  $a();
  // kết quả
  sử dụng phương thức __invoke()
  ```

- \_\_sleep() : phương thức này được sử dụng khi ta `serialize` đối tượng

  ```
      class a
      {
          private $name = 'Tran Hoang Nhat Anh';
          private $age = 20;
          public function __sleep()
          {
              return ['name'];
          }
      }
      $a = new a();
      echo serialize($a);
      // kết quả
      O:1:"a":1:{s:7:"aname";s:19:"Tran Hoang Nhat Anh";}
  ```

- \_\_wakeup() : phương thức này được sử dụng khi `unserialize` đối tượng

  ```
      class a
      {
          private $name = 'Tran Hoang Nhat Anh';
          private $age = 20;
          public function __sleep()
          {
              return ['name'];
          }

          /**
          * sử dụng hàm getName khi unserialize()
          */
          public function __wakeup()
          {
              echo "sử dụng phương thức __wakeup()";
          }
      }
      $a = new a();
      unserialize(serialize($a));

      // kết quả
      sử dụng phương thức __wakeup()
  ```

- \_\_set_state() : phương thức này được sử dụng khi `var_export` đối tượng

  ```
      class a
      {

          public static function __set_state($arr)
          {
              echo "sử dụng phương thức __set_state()";
          }
      }
      $a = new a();
      eval(var_export($a, true) . ";");
      // kết quả
      sử dụng phương thức __set_state();
  ```

- \_\_clone() : phương thức này được sử dụng khi `clone` đối tượng

  ```
  class a
  {

      public function __clone()
      {
          echo "sử dụng phương thức __clone()";
      }
  }
  $a = new a();
  $b = clone ($a);
  // kết quả
  sử dụng phương thức __clone()
  ```

- \_\_debugInfo() : phương thức này được sử dụng khi t `var_dump` đối tượng

  ```
  class a
  {

      public function __debugInfo()
      {
          echo "sử dụng phương thức __debugInfo()";
      }
  }
  $a = new a();
  var_dump($a);
  // kết quả
  sử dụng phương thức __debugInfo()object(a)#1 (0) {}
  ```

- thuật toán sắp xếp trong php:

- sort() : hàm này dùng để sắp xếp tăng dần

  ```
  $a = [5,3,2,1,0,4];
  sort($a);
  print_r($a);
  // kết quả
  Array
  (
      [0] => 0
      [1] => 1
      [2] => 2
      [3] => 3
      [4] => 4
      [5] => 5
  )
  ```

- rsort() : hàm này sử dụng để sắp xếp giảm dần

  ```
  $a = [0,5,3,2,1,5,6];
  rsort($a);
  print_r($a);
  // kết quả
  Array
  (
      [0] => 6
      [1] => 5
      [2] => 5
      [3] => 3
      [4] => 2
      [5] => 1
      [6] => 0
  )
  ```

- ksort() : hàm này sử dụng để sắp xếp theo `key` tăng dần

  ```
  $a = [31=>1,23=>1,0=>12,2=>3, 7 => 99];
  ksort($a);
  print_r($a);
  // kết quả
  Array
  (
      [0] => 12
      [2] => 3
      [7] => 99
      [23] => 1
      [31] => 1
  )
  ```

- krsort(): hàm này sử dụng để sắp xếp theo `key` giảm dần

  ```
  $a = [31=>1,23=>1,0=>12,2=>3, 7 => 99];
  krsort($a);
  print_r($a);
  // kết quả
  Array
  (
      [31] => 1
      [23] => 1
      [7] => 99
      [2] => 3
      [0] => 12
  )
  ```

- usort() : hàm này sử dụng để sắp xếp theo hàm so sánh do người dùng định nghĩa

  ```
  $a = [31=>1,23=>1,0=>12,2=>3, 7 => 99];

  function cmp($a, $b)
  {
      if ($a == $b) {
          return 1;
      }
      return ($a > $b) ? 1 : -1;
  }

      usort($a,'cmp');
      print_r($a);
      // kết quả
      Array
      (
          [0] => 1
          [1] => 1
          [2] => 3
          [3] => 12
          [4] => 99
      )
  ```

- uasort(): hàm này dùng để sắp xếp theo hàm so sánh nguời dùng định nghĩa và giữ nguyên `key` chỉ so sánh `value`

  ```
   $a = [31=>1,23=>1,0=>12,2=>3, 7 => 99];

  function cmp($a, $b)
  {
      if ($a == $b) {
          return 1;
      }
      return ($a > $b) ? 1 : -1;
  }
  uasort($a, 'cmp');
  print_r($a);
  // kết quả
  Array
  (
      [23] => 1
      [31] => 1
      [2] => 3
      [0] => 12
      [7] => 99
  )
  ```

- uksort(): hàm này dùng để sắp xếp theo hàm so sánh do người dùng định nghĩa. chỉ so sánh `key`

  ```
   $a = [31=>1,23=>1,0=>12,2=>3, 7 => 99];

  function cmp($a, $b)
  {
      if ($a == $b) {
          return 1;
      }
      return ($a > $b) ? 1 : -1;
  }

  uksort($a, 'cmp');
  print_r($a);

  // kết quả
  Array
  (
      [0] => 12
      [2] => 3
      [7] => 99
      [23] => 1
      [31] => 1
  )
  ```

- thuật toán so sánh trong php:

- array_diff() : hàm này dùng để so sánh `value` của 2 mảng và trả về 1 mảng các giá trị có trong mảng 1 mà không có trong mảng khác

  ```
  $a = [1,2,3,4,5];
  $b = [1,2,3];

  $result = array_diff($a, $b);
  print_r($result);

  // kết quả
  Array
  (
      [3] => 4
      [4] => 5
  )
  ```

- array_diff_assoc() : hàm này dùng để so sánh `key` và `value` của 2 mảng và trả về 1 mảng các giá trị có trong mảng 1 mà không có trong mảng khác

  ```
      $a1=array("a"=>"red","b"=>"green","c"=>"blue","d"=>"yellow");
      $a2=array("a"=>"red","b"=>"green","d"=>"blue");

      $result = array_diff_assoc($a1,$a2);
      print_r($result);

      // kết quả
      Array
      (
          [c] => blue
          [d] => yellow
      )
  ```

- array_diff_key() : hàm này dùng để so sánh `key` của 2 mảng và trả về 1 mảng các giá trị có trong mảng 1 mà không có trong mảng khác

  ```
      $a1=array("a"=>"red","b"=>"green","c"=>"black");
      $a2=array("a"=>"red","c"=>"blue","d"=>"pink");

      $result = array_diff_key($a1,$a2);
      print_r($result);

      // kết quả
      Array
      (
          [b] => green
      )
  ```

- array_udiff() : hàm này dùng để so sánh `value` của 2 mảng bằng hàm do người dùng định nghĩa và trả về 1 mảng các giá trị có trong mảng 1 mà không có trong mảng khác

  ```
  function myfunction($a,$b)
  {
    if ($a===$b)
    {
        return 0;
    }
        return ($a>$b)?1:-1;
  }

  $a1=array("a"=>"red","b"=>"green","c"=>"blue");
  $a2=array("a"=>"blue","b"=>"black","e"=>"blue");

  $result = array_udiff($a1,$a2,'myfunction');
  print_r($result);

  // kết quả
  Array
    (
        [a] => red
        [b] => green
    )
  ```

- array_diff_uassoc() : hàm này dùng để so sánh `key` và `value` của 2 mảng và trả về 1 mảng các giá trị có trong mảng 1 mà không có trong mảng khác
  ```
    function myfunction($a,$b)
    {
        if ($a===$b)
        {
            return 0;
        }
        return ($a>$b)?1:-1;
    }

    $a1=array("a"=>"red","b"=>"black","c"=>"blue");
    $a2=array("d"=>"red","b"=>"green","e"=>"blue");

    $result=array_diff_uassoc($a1,$a2,"myfunction");
    print_r($result);

    // kết quả
    Array
    (
        [a] => red
        [b] => black
        [c] => blue
    )
  ```
- array_diff_ukey() : hàm này dùng để so sánh `key` của 2 mảng và trả về 1 mảng các giá trị có trong mảng 1 mà không có trong mảng khác
  ```
  function myfunction($a,$b)
    {
        if ($a===$b)
        {
            return 0;
        }
        return ($a>$b)?1:-1;
    }

    $a1=array("a"=>"red","b"=>"green","c"=>"blue");
    $a2=array("a"=>"blue","b"=>"black","e"=>"blue");

    $result=array_diff_ukey($a1,$a2,"myfunction");
    print_r($result);

    // kết quả
    Array
    (
        [c] => blue
    )
  ```
- array_intersect() : hàm này dùng để so sánh `value` của 2 mảng và trả về 1 mảng chứa các giá trị của mảng 1 có trong mảng khác
  ```
    $a1=array("a"=>"red","b"=>"green","c"=>"black","d"=>"yellow");
    $a2=array("e"=>"red","f"=>"green","g"=>"blue", "c"=>"white");

    $result=array_intersect($a1,$a2);
    print_r($result);

    // kết quả
    Array
    (
        [a] => red
        [b] => green
    )
  ```
- array_intersect_assoc() : hàm này dùng để so sánh `key` và `value` của 2 mảng và trả về 1 mảng các giá trị của mảng 1 có trong mảng khác
  ```
    $a1 = array("a" => "red", "b" => "green", "c" => "blue", "d" => "yellow");
    $a2 = array("a" => "red", "e" => "green", "c" => "black");

    $result = array_intersect_assoc($a1, $a2);
    print_r($result);

    // kết quả
    Array
    (
        [a] => red
    )
  ```
- array_intersect_key() : hàm này dùng để so sánh `key` của 2 mảng và trả về 1 mảng các giá của mảng 1 có trong mảng khác
  ```
    $a1=array("a"=>"red","b"=>"green","c"=>"blue");
    $a2=array("a"=>"red","c"=>"blue","d"=>"pink");

    $result=array_intersect_key($a1,$a2);
    print_r($result);

    // kết quả
    Array
    (
        [a] => red
        [c] => blue
    )
  ```
- array_uintersect() : hàm này dùng để so sánh `value` của 2 mảng bằng hàm người dùng tự định nghĩa và trả vể 1 mảng các giá trị của mảng 1 có trong mảng khác
  ```
    function myfunction($a,$b)
    {
        if ($a===$b)
        {
            return 0;
        }
        return ($a>$b)?1:-1;
    }

    $a1=array("a"=>"red","b"=>"green","c"=>"blue");
    $a2=array("a"=>"blue","b"=>"black","e"=>"blue");

    $result=array_uintersect($a1,$a2,"myfunction");
    print_r($result);

    // kết quả
    Array
    (
        [c] => blue
    )
  ```
- array_intersect_uassoc() : hàm này dùng để so sánh `key` và `value` của 2 mảng bằng hàm người dùng tự định nghĩa và trả về 1 mảng các giá trị của mảng 1 có trong mảng khác
  ```
    function myfunction($a,$b)
    {
        if ($a===$b)
        {
            return 0;
        }
        return ($a>$b)?1:-1;
    }

    $a1=array("a"=>"red","b"=>"green","c"=>"blue");
    $a2=array("d"=>"red","b"=>"green","e"=>"blue");

    $result=array_intersect_uassoc($a1,$a2,"myfunction");
    print_r($result);

    // kết quả
    Array
    (
        [b] => green
    )
  ```
- array_intersect_ukey() : hàm này dùng để so sánh `key` của 2 mảng bằng hàm người dùng tự định nghĩa và trả về 1 mảng các giá của mảng 1 có trong mảng khác
  ```
    function myfunction($a,$b)
    {
        if ($a===$b)
        {
            return 0;
        }
        return ($a>$b)?1:-1;
    }

    $a1=array("a"=>"red","b"=>"green","c"=>"blue");
    $a2=array("a"=>"blue","b"=>"black","e"=>"blue");

    $result=array_intersect_ukey($a1,$a2,"myfunction");
    print_r($result);

    // kết quả
    Array
    (
        [a] => red
        [b] => green
    )
  ```
- thuật toán tìm kiếm trong php:
- array_search() : hàm này dùng để tìm kiếm `value` trong mảng và trả về `key`
  ```
    $a=array("a"=>"red","b"=>"green","c"=>"blue");
    
    print_r(array_search("red",$a));

    // kết quả
    a
  ```
- array_key_exists(): hàm này dùng để kiếm tra `key` có tồn tại trong mảng không
  ```
    $a = array("Volvo" => "XC90", "BMW" => "X5");

    if (array_key_exists("Volvo", $a)) {
        echo "Key exists!";
    } else {
        echo "Key does not exist!";
    }

    // kết quả
    Key exists!

    if (array_key_exists("anh", $a)) {
        echo "Key exists!";
    } else {
        echo "Key does not exist!";
    }

    // kết quả
    Key does not exist!
  ```
- in_array(): hàm này dùng để kiểm tra `value` có trong mảng hay không
  ```
    $people = array("Peter", "Joe", "Glenn", "Cleveland");

    if (in_array("Glenn", $people))
    {
        echo "Match found";
    }else {
        echo "Match not found";
    }

    // kết quả
    Match found
  ```  
- thuật toán thay thế trong php:
- array_merge(): hàm này dùng để gộp 2 mảng làm 1 và trả về 1 mảng mới
  ```
    $a1=array("red","green");
    $a2=array("blue","yellow");

    $result=array_merge($a1,$a2);
    print_r($result);

    // kết quả
    Array
    (
        [0] => red
        [1] => green
        [2] => blue
        [3] => yellow
    )
  ```
- array_push() : hàm này dùng để thêm phầm tử mới vào cuối mảng
  ```
    $a=array("red","green");
    array_push($a,"blue","yellow");

    print_r($a);

    // kết quả
    Array
    (
        [0] => red
        [1] => green
        [2] => blue
        [3] => yellow
    )
  ```
- array_map() : hàm này dùng để trả về 1 mảng mới sau khi mảng ban đầu áp dụng hàm callback
  ```
  function cube($n)
   {
      return($n * $n * $n);
   }

   $a = array(1, 2, 3, 4, 5);
   $b = array_map("cube", $a);

   print_r($b);

   // kết quả
   Array
    (
        [0] => 1
        [1] => 8
        [2] => 27
        [3] => 64
        [4] => 125
    )
  ```
- array_replace(): hàm này dùng để thay thế `value` mảng 1 bằng `value` mảng 2 nếu chúng có cùng `key`. Nếu `key` chỉ có ở mảng 2 thì được thêm vào cuối mảng 1
  ```
    $array = array(
        0=>"php",
        1=>"js",
        2=>"python",
        5=>"html"
    );
    $replacements = array(
        0 => "css",
        3 => "C#"
    );
    $replacements2 = array(
        0 => "java"
    );
    
    $result = array_replace($array, $replacements, $replacements2);
    print_r($result);

    // kết quả
    Array
    (
        [0] => java
        [1] => js
        [2] => python
        [5] => html
        [3] => C#
    )
  ```
- array_splice(): hàm này dùng để xóa 1 phần tử trong mảng và thay thế bằng 1 phần tử khác
  ```
    $a1=array("a"=>"red","b"=>"green","c"=>"blue","d"=>"yellow");
    $a2=array("a"=>"purple","b"=>"orange","e"=>"orange");

    array_splice($a1,1,3,$a2);
    print_r($a1);

    // kết quả
    Array
    (
        [a] => red
        [0] => purple
        [1] => orange
        [2] => orange
    )
  ```
- php 8:
- Union types: biến có thể có nhiều kiểu giá trị
    ```
    public function foo(Foo|Bar $input): int|float;
    ```
- Toán tử nullsafe: 
  ```
    $startDate = $booking->getStartDate();
    // kiểm tra null kiểu cũ
    $dateAsString = $startDate ? $startDate->asDateTimeString() : null;

    // php 8
    $dateAsString = $booking->getStartDate()?->asDateTimeString();
  ```
- Gán giá trị tham số: các đối số được đặt tên cho phép truyền giá trị theo tên thay vì vị trí, có thể bỏ qua các tham số tùy chọn
  ```
    function foo(string $a, string $b, ?string $c = null, ?string $d = null) 
    { /* … */ }

    foo(b: 'value b', a: 'value a', d: 'value d');
  ```

