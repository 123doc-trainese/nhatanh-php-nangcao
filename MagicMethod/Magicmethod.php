<?php
class A
{
    public $text;
    /**
     * khởi tạo giá trị đối tượng trước khi sử dụng
     *
     * @param  $text
     * @return void
     */
    public function __construct($text)
    {
        $this->text = $text;
        echo "khởi tạo đối tượng $text \n \n";
        echo "---------------------------------------------------- \n \n";
    }

    /**
     * hủy đối tượng khi kết thúc trước trình hoặc khai báo mới đối tượng
     *
     * @return void
     */
    public function __destruct()
    {
        echo " hủy đối tượng $this->text \n \n";
    }
}

// khởi tạo A
$test = new A('tao');

class B
{
    private $name;
    /**
     * gán giá trị cho 1 thuộc tính không được phép truy cập
     *
     * @param  mixed $key
     * @param  mixed $value
     * @return void
     */
    public function __set($key, $value)
    {
        if (property_exists($this, $key)) {
            $this->$key = $value;
            echo "gọi __set \n \n";
        } else {

            echo " thuoc tinh khong ton tai \n \n";
        }
    }

    public function getName()
    {

        echo "$this->name \n \n";
    }
}

//khởi tạo B
echo "sử dụng __set \n \n";
$test2 = new B();
// gán giá trị cho thuộc tính private
$test2->name = 'tranhoangnhatanh';

$test2->getName();

echo "---------------------------------------------------- \n \n";

class C
{
    private $name = 'tran hoang nhat anh';
    /**
     * lấy giá trị từ 1 thuộc tính không được phép truy cập
     *
     * @param  mixed $key
     * @return void
     */
    public function __get($key)
    {
        if (property_exists($this, $key)) {
            return $this->$key;
        } else {

            echo "thuoc tinh khong ton tai \n";
        }
    }

    public function getName()
    {

        echo $this->name;
    }
}

// khởi tạo đối tượng

echo "sử dụng __get \n \n";
$c = new C();

//lấy giá trị thuộc tính private
echo $c->name . "\n";
echo "---------------------------------------------------- \n \n";
class D
{

    private $name = 'tran hoang nhat anh';
    private $age = '22';

    private static function getInfor()
    {
        echo ' a';
    }

    /**
     * dùng khi gọi 1 phương thức không được phép truy cập
     * @param  mixed $methodName
     * @param  mixed $arguments
     * @return void
     */
    public function __call($methodName, $arguments)
    {
        echo 'Bạn vừa gọi phương thức: ' . $methodName . ' và có các tham số: ' . implode('-', $arguments) . "\n \n";
    }

    private function getInfo()
    {
        echo $this->name . ' + ' . $this->age;
    }

    /**
     * dùng khi gọi 1 phương thức không được phép truy cập trong phạp vi của 1 static
     *
     * @param  mixed $methodName
     * @param  mixed $arguments
     * @return void
     */
    public static function __callStatic($methodName, $arguments)
    {

        echo ' Bạn vừa gọi phương thức static: ' . $methodName . ' và có các tham số: ' . implode('-', $arguments) . "\n \n";
    }
}
// khởi tạo đối tượng
echo "sử dụng __call và __callStatic \n \n";
$s = new D();
// gọi phưowng thức private 
$s->getInfo('name', 'age');
// gọi phương thức tĩnh private 
$s::getInfor('name');
echo "---------------------------------------------------- \n \n";
class E
{
    public $name = 'tran hoang nhat anh';
    private $age = '22';
    private $address = 'hanoi';
    /**
     * khi gọi hàm isset(), empty() của 1 thuộc tính không được phép truy cập
     *
     * @param  mixed $name
     * @return void
     */
    public function __isset($name)
    {

        echo (' vua kiem tra thuoc tinh:' . $name . "\n \n");
    }

    /**
     * __toString() sẽ được gọi khi chúng ta dùng đối tượng như một string.
     *
     * @return void
     */
    public function __toString()
    {
        return '  Phương thức __toString() được gọi' . "\n \n";
    }



    /**
     * dùng khi cố gọi 1 đối tượng như hàm
     *
     * @return void
     */
    public function __invoke()
    {

        echo 'Phương thức __invoke() được gọi' . "\n \n";
    }

    /**
     * được gọi khi serialize() một đối tượng. serialize() một đối tượng thì nó sẽ trả về tất cả các thuộc tính trong đối tượng đó. phương thức __sleep() luôn trả về giá trị là một mảng.
     *
     * @return void
     */
    public function __sleep()
    {
        return array('name');
    }

    public function getName()
    {
        echo $this->name;
    }

    /**
     * được gọi khi unserialize() một đối tượng.
     *
     * @return void
     */
    public function __wakeup()
    {
        $this->getName();
    }

    /**
     * gọi khi var_export() đối tượng
     *
     * @param  mixed $properties
     * @return void
     */
    public static function __set_state(array $arr)
    {
        foreach ($arr as $key => $value) {
            echo $key . '->' . $value . "\n";
        }
    }

    /**
     * dùng khi clone 1 object
     *
     * @return void
     */
    public function __clone()
    {
        echo 'Phương thức __clone() được gọi' . "\n";
    }

    /**
     * sử dụng khi sử dụng hàm var_dump()
     *
     * @return void
     */
    public function __debugInfo()
    {
        return [
            'name' => $this->name,
        ];
    }
}
// khởi tạo đối tượng
$e = new E();

//dùng hàm empty() -> gọi __isset 
echo "sử dụng __isset \n \n";
empty($e->name);
echo "---------------------------------------------------- \n \n";

// dùng đối tượng như 1 chuỗi -> gọi __toString()
echo $e;
echo "sử dụng _invoke \n \n";
//dùng đối tượng như 1 hàm ->   gọi __invoke()
$e();
echo "---------------------------------------------------- \n \n";
// serialize đối tượng -> gọi __sleep()

echo "sử dụng __sleep \n\n";
echo serialize($e);
echo "---------------------------------------------------- \n \n";
// unserialize đối tượng -> gọi __wakeup()
echo "sử dụng __wakeup \n\n";
unserialize(serialize($e));
echo "---------------------------------------------------- \n \n";
//var_export đối tượng -> gọi __set_state()

echo "sử dụng __set_state \n\n";
var_export($e, true);
echo "---------------------------------------------------- \n \n";

// clone đối tượng -> gọi __clone()
echo "sử dụng __clone \n\n";
$clone = clone ($e);
echo $clone->name;
echo "---------------------------------------------------- \n \n";

//var_dump đối tượng    -> gọi  __debugInfo()

echo "sử dụng __debugInfo \n\n";
var_dump($e);
echo "---------------------------------------------------- \n \n";