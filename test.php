<?php
$s = "abcdabcefght";
$result = '';
$i = 0;
$j = 0;
$max = 0;
// echo $s[2];
while ($j < strlen($s) && $i < strlen($s)) {
    if (strpos($result, $s[$j]) == '') {
        $result = $result . $s[$j];
        $max = ($j - $i + 1) > $max ? ($j - $i + 1) : $max;
        $j++;
    } else {
        $result = substr($result, 1);
        $i++;
    }
}

echo $max . "\n";

define('QUX', 2); // invalid
const FOO = 4 << 3;
define('QUX2', 2 * 2); //
define('qux2', 2 * 6); //
echo FOO;
// echo QUX2;
// echo QUX;

for ($i = 0; $i < 32; ++$i) {
    define('BIT_' . $i, 2 << $i);
}
echo BIT_4;